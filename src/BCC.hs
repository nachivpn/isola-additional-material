{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ConstraintKinds #-}

module BCC (
  -- * Simplicity's types and BCC objects
  -- ** Unit/Terminal
  T
  -- ** Products
  , (:*:)
  -- ** Coproducts
  , (:+:)

  -- * An EDSL for BCC
  , Mph (..)

  -- * Miscellaneous
  , prodFlip
  , o
  , x
  ) where

data T       -- Unit/Terminal
data a :*: b -- Products
data a :+: b -- Coproducts

-- | Morphisms are parametrized over a type constraint @obj@ and
-- objects @a@ and @b@
data Mph obj a b where
    -- Id, and composition (present in every category)
    Id       :: obj a => Mph obj a a
    O        :: (obj a, obj b, obj c) =>
                Mph obj b c -> Mph obj a b -> Mph obj a c
    -- When having products
    Factor   ::  (obj a, obj b1, obj b2, obj (b1 :*: b2)) =>
                 Mph obj a b1 -> Mph obj a b2 -> Mph obj a (b1 :*: b2)
    -- Pairs projections
    Fst      :: (obj a, obj b, obj (a :*: b)) => Mph obj (a :*: b) a
    Snd      :: (obj a, obj b, obj (a :*: b)) => Mph obj (a :*: b) b
    -- Terminals
    Terminal :: obj a => Mph obj a T
    -- Coproducts
    Inj1     :: (obj a, obj b, obj (a :+: b)) => Mph obj a (a :+: b)
    Inj2     :: (obj a, obj b, obj (a :+: b)) => Mph obj b (a :+: b)
    Copair   :: (obj a, obj b, obj c, obj e, obj (e :*: a), obj (e :*: b),
                 obj (a :+: b), obj (e :*: (a :+: b))) =>
                 Mph obj (e :*: a) c -> Mph obj (e :*: b) c ->
                 Mph obj (e :*: (a :+: b)) c

-- | Converts morphisms from @Mph@ to readable @String@s
instance Show (Mph obj a b) where
    show Id           = "Id"
    show (O g f)      = "(" ++ show g ++ " o " ++ show f ++ ")"
    show (Factor f g) = "<" ++ show f ++ ", " ++ show g ++ ">"
    show Fst          = "Fst"
    show Snd          = "Snd"
    show Terminal     = "Terminal"
    show Inj1         = "Inj1"
    show Inj2         = "Inj2"
    show (Copair f g) = "[" ++ show f ++ ", " ++ show g ++ "]"

-- | Products are symmetric wrt isomorphism
prodFlip :: (obj a, obj e, obj (a :*: e), obj (e :*: a)) =>
            Mph obj (a :*: e) (e :*: a)
prodFlip = Factor Snd Fst

-- | An alias for composition @O@
o :: (obj a, obj b, obj c) =>  Mph obj b c -> Mph obj a b -> Mph obj a c
o = O

-- | An alias for @Factor@
x :: (obj a, obj b1, obj b2, obj (b1 :*: b2)) =>
     Mph obj a b1 -> Mph obj a b2 -> Mph obj a (b1 :*: b2)
x = Factor
