{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables,TypeOperators #-}
module BCC2SBM where

import BCC
import SBM
import Simplicity

-- | Operational semantics of @Mph@ morphisms in the Stateful Bit MAchine
bcc2sbm :: Mph SType a b -> SBM ()
bcc2sbm (Id :: Mph SType a b) = do
        copy (sizeOf (undefined :: a))
bcc2sbm ((g :: Mph SType b c) `O` (f :: Mph SType a b)) = do
        newFrame $ sizeOf (undefined :: b)
        bcc2sbm f
        moveFrame
        bcc2sbm g
        dropFrame
bcc2sbm (Terminal)      = nop
bcc2sbm (Inj1 :: Mph SType a ab)          = do
        write False
        skip $ padl (undefined :: ab)
        copy $ sizeOf (undefined :: a)
bcc2sbm (Inj2 :: Mph SType b ab)          = do
        write True
        skip $ padl (undefined :: ab)
        copy $ sizeOf (undefined :: b)
bcc2sbm (Copair -- :: Mph (e :*: (a :+: b)) c
            (p :: Mph SType (e :*: a) c)
            (q :: Mph SType (e :*: b) c)) = do
        let sizeOfE = sizeOf (undefined :: e)
        fwd sizeOfE       -- fwd to end of e
        mbit <- readFrame -- read inl/inr flag bit to check if value
                          -- is 'a' or 'b'
        bwd sizeOfE       -- bwd to beg of e
        case mbit of
            (Just False) -> do                  -- value is a
                let x = sizeOfE + 1 + padl (undefined :: (a :+: b))
                newFrame $ sizeOf (undefined :: (e :*: a)) -- new frame to
                                                           -- write e :*: a
                copy sizeOfE                    -- copy e
                fwd x                           -- fwd to a
                copy $ sizeOf (undefined :: a)  -- copy a
                moveFrame                       -- move (e :*: a) from
                                                -- write to read
                bcc2sbm p                       -- run p (as read stack has
                                                -- value of type e :*: a)
                dropFrame                       -- drop frame containing e :*: a
                bwd x                           -- bwd to beginning
            (Just True)  -> do                  -- value is b
                let x = sizeOfE + 1 + padr (undefined :: (a :+: b))
                newFrame $ sizeOf (undefined :: (e :*: b)) -- new frame to write e :*: b
                copy sizeOfE                    -- copy e
                fwd x                           -- fwd to b
                copy $ sizeOf (undefined :: b)  -- copy b
                moveFrame                       -- move (e :*: b) from
                                                -- write to read
                bcc2sbm q                       -- run q (as read stack has
                                                -- value of type e :*: b)
                dropFrame                       -- drop frame containing e :*: b
                bwd x                           -- bwd to beginning
bcc2sbm (Factor -- :: Mph SType a (b :*: c)
                (p :: Mph SType a b)
                (q :: Mph SType a c)) = do
        bcc2sbm p
        bcc2sbm q
bcc2sbm (Fst :: Mph SType ab a)     = do
    copy (sizeOf (undefined :: a))
bcc2sbm (Snd :: Mph SType ab b)     = do
    fwd (bsizf (undefined :: ab))
    copy (sizeOf (undefined :: b))
    bwd (bsizf (undefined :: ab))
