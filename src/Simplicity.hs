{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE TypeFamilies#-}

module Simplicity (
 -- * An EDSL for Simplicity
 Simpl (..)
 , SType (..)
 , SumTypes (..)
 , ProductTypes (..)

 -- * Denotational Semantics
 , Hask
 , ds
 ) where

import Data.Either
import BCC ((:*:),(:+:),T)

-- | Simplicity terms are parameterized over an input type @i@ and an
-- output type @o@
data Simpl i o where
    Iden :: SType a => Simpl a a
    Comp :: (SType a, SType b, SType c) => Simpl a b -> Simpl b c -> Simpl a c
    Unit :: SType a => Simpl a T
    Injl :: (SType a, SType b, SType c) => Simpl a b -> Simpl a (b :+: c)
    Injr :: (SType a, SType b, SType c) => Simpl a c -> Simpl a (b :+: c)
    Case :: (SType a, SType b, SType c, SType d) => Simpl (a :*: c) d ->
            Simpl (b :*: c) d -> Simpl ((a :+: b) :*: c) d
    Pair :: (SType a, SType b, SType c) => Simpl a b -> Simpl a c ->
            Simpl a (b :*: c)
    Take :: (SType a, SType b, SType c) => Simpl a c -> Simpl (a :*: b) c
    Drop :: (SType a, SType b, SType c) => Simpl b c -> Simpl (a :*: b) c

-- | Mapping Simplicity types into Haskell types
type family Hask i :: * where
  Hask T          = ()
  Hask (b :*: c)  = (Hask b, Hask c)
  Hask (b :+: c)  = Either (Hask b) (Hask c)

-- | Denotational semantics
ds :: Simpl i o -> (Hask i -> Hask o)
ds Iden       = id
ds (Comp s t) = ds t . ds s
ds Unit       = \_ -> ()
ds (Injl t)   = Left . ds t
ds (Injr t)   = Right . ds t
ds (Pair s t) = \x -> (ds s x, ds t x)
ds (Take t)   = ds t . fst
ds (Drop t)   = ds t . snd
ds (Case s t) = \x -> case x of
                        (Left a,c)  -> ds s (a,c);
                        (Right a,c) -> ds t (a,c);

-- | Type class that is implemented only by Simplicity types
class SType a where
  sizeOf :: a -> Int -- ^ Calculates the size (in bits) of a Simplicity type

-- | Type class that is implemented only for Sum types
class SumTypes a where
  padl  :: a -> Int -- ^ Number of undefined values that need to be
                    -- padded for the left injection
  padr  :: a -> Int -- ^ Number of undefined values that need to be
                    -- padded for the right injection

-- | Type class that is implemented only for Product types
class ProductTypes a where
  bsizf :: a -> Int -- Size of the first component of a pair
  bsizs :: a -> Int

-- | Unit does not need any bits to represent its value.
instance SType T where
  sizeOf _ = 0

-- | A sum type value needs one bit for its tag and enough bits to
-- represent either @a@ or @b@
instance (SType a, SType b) => SType (a :+: b) where
  sizeOf _ = let a = sizeOf (undefined :: a)
                 b = sizeOf (undefined :: b)
             in 1 + max a b

-- | A value of product type needs space to hold both its values @a@ and @b@
instance (SType a, SType b) => SType (a :*: b) where
  sizeOf _ = let a = sizeOf (undefined :: a)
                 b = sizeOf (undefined :: b)
             in a + b

instance (SType a, SType b) => ProductTypes (a :*: b) where
  bsizf _ = sizeOf (undefined :: a)
  bsizs _ = sizeOf (undefined :: b)

instance (SType a, SType b) => SumTypes (a :+: b) where
  padl _ = let a = sizeOf (undefined :: a)
               b = sizeOf (undefined :: b)
           in (max a b) - a
  padr _ = let a = sizeOf (undefined :: a)
               b = sizeOf (undefined :: b)
           in (max a b) - b
