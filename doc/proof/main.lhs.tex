\documentclass[runningheads]{article}

%include polycode.fmt

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{authblk}

\usepackage[fleqn]{amsmath}
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{enumerate}

\newtheorem{theorem}{Theorem}[section]

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

%% Notation Macros
\newcommand{\ds}[1]{\ensuremath{\llbracket #1 \rrbracket}}

%% Simplicity's primitive
\newcommand{\Iden}{\ensuremath{\textrm{Iden}}}
\newcommand{\Comp}{\ensuremath{\textrm{Comp}}}
\newcommand{\Pair}{\ensuremath{\textrm{Pair}}}
\newcommand{\Take}{\ensuremath{\textrm{Take}}}
\newcommand{\Drop}{\ensuremath{\textrm{Drop}}}
\newcommand{\Unit}{\ensuremath{\textrm{Unit}}}
\newcommand{\Injl}{\ensuremath{\textrm{Injl}}}
\newcommand{\Injr}{\ensuremath{\textrm{Injr}}}
\newcommand{\Case}{\ensuremath{\textrm{Case}}}

\newcommand{\id}{\ensuremath{\textrm{id}}}
\newcommand{\fst}{\ensuremath{\textrm{fst}}}
\newcommand{\snd}{\ensuremath{\textrm{snd}}}
\newcommand{\Left}{\ensuremath{\textrm{Left}}}
\newcommand{\Right}{\ensuremath{\textrm{Right}}}


%format family = "\mathbf{family}"
%format (ds (x)) = "\llbracket " x " \rrbracket "
%format Comp = "\Comp"
%format Iden = "\Iden"
%format Take = "\Take"
%format Drop = "\Drop"
%format Pair = "\Pair"
%format Unit = "\Unit"
%format Injl = "\Injl"
%format Injr = "\Injr"
%format Case = "\Case"


%format id   = "\id"
%format fst  = "\fst"
%format snd  = "\snd"
%format Left = "\Left"
%format Right = "\Right"
%format prod = "\!:\!*\!:\!"
%format plus = "\!:\!+\!:\!"
%format iff  = "\Leftrightarrow"
%format ^^   = "\;"
%format pi   = "\pi"
%format pi1
%format pi2
%format i    ="i"
%format i1
%format i2
%format x0   = x"_0"

\begin{document}

\title{\bf Towards Adding Variety to Simplicity \\
  \large Additional Material: Proofs}

\author[1]{Nachiappan Valliappan}
\author[2]{Sol\`ene Mirliaz}
\author[1]{Elisabet Lobo Vesga}
\author[1]{Alejandro Russo}

\affil[1]{Chalmers University of Technology}
\affil[2]{ENS Rennes}

\date{}


\maketitle

\section{Formalization}

\begin{definition}[Denotational semantics]
  $ $
  \begin{code}
    type family Hask i :: ^^ * ^^ where
      Hask T           = ()
      Hask (b prod c)  = (Hask b, Hask c)
      Hask (b plus c)  = Either (Hask b) (Hask c)
    ^^
    ds            :: Simpl i o -> (Hask i -> Hask o)
    ds Iden        = id
    ds (Comp s t)  = ds t . ds s
    ds (Pair s t)  = \x -> (ds s x, ds t x)
    ds (Take t)    = ds t . fst
    ds (Drop t)    = ds t . snd
    ds Unit        = \_ -> ()
    ds (Injl t)    = Left . ds t
    ds (Injr t)    = Right . ds t
    ds (Case s t)  = \x -> case x of (Left a,c) -> ds s (a,c); (Right a,c) -> ds t (a,c);
  \end{code}
\end{definition}

\begin{definition}[Equivalence kernel]
  We introduce the \emph{equivalence kernel} of the function |ds| as
  the equivalence relation |(==)| defined by |s == t iff (ds s) == (ds
  t)|
\end{definition}


\begin{theorem}[Simplicity is a Category]
  The category {\bf Simplicity} has types as objects and terms as
  morphisms. Composition of Simplicity terms is defined by the term
  |Comp| while identity morphisms are |Iden| terms.
\end{theorem}

To see that {\bf Simplicity} is a category we need to check that the
following laws hold:

\begin{itemize}
\item For any term |t :: Simpl A B|, the identity functions |Iden ::
  Simpl A A| and |Iden :: Simpl B B| satisfy the respective equations
  |Comp Iden t == t| and |Comp t Iden == t|.

  \begin{proof}[Left identity]
    $ $ % New line
    \begin{spec}
      ds (Comp t Iden)
      -- by def of |ds Comp|
      == (ds Iden) . (ds t)
      -- by def of |ds Iden|
      == id . (ds t)
      -- by applying |.|
      == ds t
      => Comp t Iden == t
    \end{spec}
  \end{proof}

  \begin{proof}[Right Identity]
    $ $ % New line
    \begin{spec}
      ds (Comp Iden t)
      -- by def of |ds Comp|
      == (ds t) . (ds Iden)
      -- by def of |ds Iden|
      == (ds t) . id
      -- by applying |.|
      == ds t
      => Comp Iden t  == t
    \end{spec}
  \end{proof}

\item Composition of simplicity terms is associative: for any terms |t  :: Simpl A B| |s :: Simpl B C| and |r :: Simpl C D|, we have that
  |Comp t (Comp s r) == Comp (Comp t s) r |.

  \begin{proof}[Associativity]
    $ $
    \begin{spec}
      ds (Comp t (Comp s r))
      -- by def of |ds Comp|
      == (ds (Comp s r)) . (ds t)
      -- by def of |ds Comp| 
      == ((ds r) . (ds s)) . (ds t)
      -- by associativity of |.|
      == (ds r) . ((ds s) . (ds t))
      -- by def of |ds Comp|
      == ds r . (ds (Comp t s))
      -- by def of |ds Comp|
      == ds (Comp (Comp t s) r )
      => Comp t (Comp s r) t == Comp (Comp t s) r
    \end{spec}
  \end{proof}
\end{itemize}

\begin{theorem}[Simplicity has products]
  For any two objects $A$ and $B$ in {\bf Simplicity}, there exists a
  product object |A prod B|, together with two projections |pi1 ::
  Simpl (A prod B) A| and |pi2 :: Simpl (A prod B) B| such that for
  any object $C$ and terms |s :: Simpl C A|, |t :: Simpl C B| there is
  exactly one morphism |u :: Simpl C (A prod B)| satisfying the
  following laws:

  \begin{enumerate}
  \item |pi1 . u == s|
  \item |pi2 . u == t|
  \item |u| is unique.
  \end{enumerate}
\end{theorem}

\begin{proof}{}
  We define the projections |pi1|, |pi2| and the function |u| as
  follows:
  \begin{code}
    pi1  = Take Iden  :: Simpl (A prod B) A
    pi2  = Drop Iden  :: Simpl (A prod B) B
    u    = Pair s t   :: Simpl C (A prod B)
  \end{code}

  \begin{enumerate}
  \item
    \begin{spec}
      ds (pi1 . u)
      -- by def of |ds (.)|
      == (ds pi1) . (ds u)
      -- by def of |ds (Take Iden)| and |ds Iden|
      == (id . fst) . (ds u)
      -- by applying |.|
      == fst . (ds u)
      -- by def of |u|
      == fst. (ds (Pair s t))
      -- by def of |.|
      == \x -> fst ((ds (Pair s t)) x)
      -- by def of |ds Pair|
      == \x -> fst ((ds s) x , (ds t) x)
      -- by def of |fst|
      == \x -> (ds s) x
      -- by $\eta$-conversion
      == ds s
      => pi1 . u == s
    \end{spec}
  \item
    \begin{spec}
      ds (pi2 . u)
      -- by def of |ds (.)|
      == (ds pi1) . (ds u)
      -- by def of |ds (Drop Iden)| and |ds Iden|
      == (id . snd) . (ds u)
      -- by applying |.|
      == snd . (ds u)
      -- by def of |u|
      == snd. (ds (Pair s t))
      -- by def of |.|
      == \x -> snd ((ds (Pair s t)) x)
      -- by def of |ds Pair|
      == \x -> snd ((ds s) x , (ds t) x)
      -- by def of |snd|
      == \x -> (ds t) x
      -- by $\eta$-conversion
      == ds t
      => pi2 . u == t
    \end{spec}

  \item Suppose there exists |u' : Simpl C (A prod B)| such that (i)
    |pi1 . u' == s| and (ii) |pi2 . u' == t|, then, there must
    be the case that |u == u'|

    \begin{spec}
      ds u
      -- by def of u
      == ds (Pair s t)
      -- by def of |ds Pair|
      == \x -> ((ds s) x , (ds t) x)
      -- by (i) and (ii)
      == \x -> ((ds (pi1 . u')) x , (ds (pi2 . u')) x)
      -- by def of |ds .|
      == \x -> (((ds pi1) . (ds u')) x , ((ds pi2) . (ds u')) x)
      -- by def of |.|
      == \x -> ((ds pi1) ((ds u') x)) , (ds pi2) ((ds u') x))
      -- by def of |ds (Take Iden)| and |ds (Drop Iden)|
      == \x -> ((id . fst) ((ds u') x)) , (id . snd) ((ds u') x))
      -- by applying |.|
      == \x -> (fst ((ds u') x)) , snd ((ds u') x))
      -- by renaming |(ds u') x == (p , q)|
      == \x -> (fst (p , q) , snd (p, q))
      -- by def of |fst| and |snd|
      == \x -> (p,q)
      -- by def of |(p,q)|
      == \x -> (ds u') x
      -- by $\eta$-conversion
      == ds u'
      => u == u'
    \end{spec}
  \end{enumerate}
\end{proof}

\begin{theorem}[Simplicity has coproducts]
  For any two objects $A$ and $B$ in {\bf Simplicity}, there exists a
  coproduct object |A plus B|, together with two injections |i1 ::
  Simpl A (A plus B)| and |i2 :: Simpl B (A plus B)| such that for
  any object $C$ and terms |s :: Simpl A C|, |t :: Simpl B C| there is
  exactly one morphism |v :: Simpl (A plus B) C| satisfying the
  following laws:

  \begin{enumerate}
  \item |v . i1 == s|
  \item |v . i2 == t|
  \item |v| is unique.
  \end{enumerate}
\end{theorem}

\begin{proof}{}
  We define the injections |i1|, |i2| and the function |v| as follows:
  \begin{code}
    i1  = Injl Iden                                       :: Simpl A (A plus B)
    i2  = Injr Iden                                       :: Simpl B (A plus B)
    v   = (Case (Take s) (Take t)) . (Pair Iden Iden)  :: Simpl (A plus B) C
  \end{code}

  \begin{enumerate}
  \item
    \begin{spec}
      ds (v . i1)
      -- by def of |ds (.)|
      == (ds v) . (ds i1)
      -- by def of |i1|
      == (ds v) . (ds (Injl Iden))
      -- by def of |ds Injl| and |ds Iden|
      == (ds v) . (Left . id)
      -- by def of |.|
      == (ds v) . Left
      -- by def of |v|
      == (ds ((Case (Take s) (Take t)) . (Pair Iden Iden))) . Left
      -- by def of |ds .|
      == ds (Case (Take s) (Take t)) . ds (Pair Iden Iden) . Left
      -- by def of |ds Pair|
      == ds (Case (Take s) (Take t)) . (\x -> ((ds Iden) x, (ds Iden) x)) . Left
      -- by def of |ds Iden|
      == ds (Case (Take s) (Take t)) . (\x -> (id x , id x)) . Left
      -- by def of |id|
      == ds (Case (Take s) (Take t)) . (\x -> (x , x)) . Left
      -- by def of |.|
      == ds (Case (Take s) (Take t)) . (\y -> (\x -> (x , x)) (Left y))
      -- by applying |Left y|
      == ds (Case (Take s) (Take t)) . (\y -> (Left y, Left y))
      -- by def of |.|
      == \y -> (ds (Case (Take s) (Take t)) (Left y, Left y))
      -- by def of |ds Case|
      == \y -> (ds (Take s) (y, Left y))
      -- by def of |ds Take|
      == \y -> (ds s . fst) (y, Left y)
      -- by def of |fst| and |.|
      == \y -> (ds s) y
      -- by $\eta$-conversion
      == ds s
    \end{spec}

    \newpage

    \item
    \begin{spec}
      ds (v . i2)
      -- by def of |ds (.)|
      == (ds v) . (ds i2)
      -- by def of |i2|
      == (ds v) . (ds (Injr Iden))
      -- by def of |ds Injr| and |ds Iden|
      == (ds v) . (Right . id)
      -- by def of |.|
      == (ds v) . Right
      -- by def of |v|
      == (ds ((Case (Take s) (Take t)).  (Pair Iden Iden))) . Right
      -- by def of |ds .|
      == ds (Case (Take s) (Take t)) . ds (Pair Iden Iden) . Right
      -- by def of |ds Pair|
      == ds (Case (Take s) (Take t)) . (\x -> ((ds Iden) x, (ds Iden) x)) . Right
      -- by def of |ds Iden|
      == ds (Case (Take s) (Take t)) . (\x -> (id x , id x)) . Right
      -- by def of |id|
      == ds (Case (Take s) (Take t)) . (\x -> (x , x)) . Right
      -- by def of |.|
      == ds (Case (Take s) (Take t)) . (\y -> (\x -> (x , x)) (Right y))
      -- by applying |Right y|
      == ds (Case (Take s) (Take t)) . (\y -> (Right y, Right y))
      -- by def of |.|
      == \y -> (ds (Case (Take s) (Take t)) (Right y, Right y))
      -- by def of |ds Case|
      == \y -> (ds (Take t) (y, Right y))
      -- by def of |ds Take|
      == \y -> (ds t . fst) (y, Right y)
      -- by def of |fst| and |.|
      == \y -> (ds t) y
      -- by $\eta$-conversion
      == ds t
    \end{spec}

    \item Suppose there exists |v' : Simpl (A plus B) C| such that (i)
    |v' . i1 == s| and (ii) |v' . i2 == t|, then, there must be the
    case that |v == v'|.

    We prove this by pattern matching over the arguments of |ds v|
    relying on function extensionality (i.e.,
    $\forall x.~v~(x) \equiv v'~(x) \Leftrightarrow v \equiv v'$)

    \begin{spec}
      (ds v) (Left a)
      -- by def of |v|
      == (ds ((Case (Take s) (Take t)) . (Pair Iden Iden))) (Left a)
      -- by def of |ds (.)|
      == (ds (Case (Take s) (Take t)) . ds (Pair Iden Iden)) (Left a)
      -- by def of |ds Pair|, |Iden| and |.|
      ==  ds (Case (Take s) (Take t)) (Left a, Left a)
      -- by def of |ds Case|
      == ds (Take s) (a, Left a)
      -- by def of |ds Take|
      == (ds s . fst) (a , Left a)
      -- by def of |fst| and |.|
      == (ds s) a
      -- by (i)
      == ds (v' .  i1) a
      -- by def of |ds .|
      == (ds v' . ds i1) a
      -- by def of |i1|
      == (ds v' . ds (Injl Iden)) a
      -- by def of |ds Injl| and |ds Iden|
      == (ds v' . Left . id) a
      -- by applying a
      == ds v' (Left a)
      => v (Left a) == v' (Left a)
    \end{spec}

    \begin{spec}
      (ds v) (Right b)
      -- by def of |v|
      == (ds ((Case (Take s) (Take t)) . (Pair Iden Iden))) (Right b)
      -- by def of |ds (.)|
      == (ds (Case (Take s) (Take t)) . ds (Pair Iden Iden)) (Right b)
      -- by def of |ds Pair|, |Iden| and |.|
      ==  ds (Case (Take s) (Take t)) (Right b, Right b)
      -- by def of |ds Case|
      == ds (Take t) (b , Right b)
      -- by def of |ds Take|
      == (ds t . fst) (b , Right b)
      -- by def of |fst| and |.|
      == (ds t) b
      -- by (ii)
      == ds (v'.  i2) b
      -- by def of |ds .|
      == (ds v' . ds i2) b
      -- by def of |i2|
      == (ds v' . ds (Injr Iden)) b
      -- by def of |ds Injr| and |ds Iden|
      == (ds v' . Right . id) b
      -- by applying b
      == ds v' (Right b)
      => v (Right b) == v' (Right b)
    \end{spec}
  \end{enumerate}
\end{proof}
\end{document}
